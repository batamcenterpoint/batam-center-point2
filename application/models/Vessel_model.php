<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Vessel_model extends CI_Model {

    public function getAllVessel() {
        return $this->db->get('daftar_vessel');
    }

    public function getVesselById($id){
        return $this->db->get_where(['id' => $id]);
    }

    public function insertVessel($data) {
        $this->db->insert('daftar_vessel', $data);
    }

    

}
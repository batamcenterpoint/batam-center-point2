<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_model extends CI_Model {

    public function getAllAgent() {
        return $this->db->get('daftar_agen');
    }

    public function getAgentById($id){
        return $this->db->get_where(['id' => $id]);
    }

    public function insertCustomer($data) {
        $this->db->insert('daftar_agen', $data);
    }

    

}
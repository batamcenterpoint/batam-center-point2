<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Agent_model', 'agent');
        $this->load->model('Wilayah_model', 'provinsi');
    }

    public function index(){
        $resultAgent = $this->agent->getAllAgent()->result_array();
        $data = [
            'dataAgent' => $resultAgent
        ];
        $this->load->view('agent', $data);
    }

    public function add(){
        $this->load->view('addAgent');
    }

    public function simpan_agent(){

        // 2. Tangkap data dari form
        $insert_t_customer = [
            'nama_customer' => $this->input->post('namaCustomer'),
            'no_telp' => $this->input->post('noTelp'),
            'email' => $this->input->post('email'),
            'alamat' => $this->input->post('alamat'),
            'provinsi' => $this->input->post('provinsi'),
            'kota' => $this->input->post('kota'),
            'kecamatan' => $this->input->post('kecamatan'),
            'negara' => $this->input->post('negara'),
            'kode_pos' => $this->input->post('kodepos'),
            'max_kredit' => $this->input->post('maxKredit'),
            'tempo_kredit' => $this->input->post('tempoKredit')
        ];

        // 3. Simpan ke database
        $this->customer->insertCustomer($insert_t_customer);

        // 4. Alihkan ke halaman produk
        redirect('customer');

    }


}

?>